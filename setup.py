from distutils.core import setup


setup(
    name="strangeweb",
    version="0.0.1",
    packages=[
        "strangeweb",
    ],
    entry_points={
        'console_scripts': [
            "strangeweb = strangeweb.server:main",
        ]
    },
    install_requires=[
        "tornado==3.2",
    ],
    author="Joel Watts",
    author_email="joel@joelwatts.com",
    url="https://bitbucket.org/jpwatts/strangeweb",
    classifiers=[
        "Programming Language :: Python :: 3",
    ]
)
