(function(global, undefined) {

var get = Ember.get,
    set = Ember.set,
    isNone = Ember.isNone;


var App = global.App = Ember.Application.create();


App.State = Ember.ObjectProxy.extend();


App.Events = Ember.Object.extend({
    _source: null,

    connect: function() {
        var source = get(this, '_source');

        if (! isNone(source)) {
            return;
        }

        source = new EventSource(App.eventsUrl);

        var _this = this;

        source.addEventListener('state', function(e) {
            var message = e.data,
                state = JSON.parse(message),
                version = state.version;
//            console.debug("%@ state\n%@".fmt(APP_EVENT_SOURCE_URL, message));

            if (version != App.version) {
                App.version = version;
                applicationCache.update();
            }

            state.time = new Date(state.time);
            set(_this, 'state.content', state);
        }, false);

        source.addEventListener('alert', function(e) {
            var message = e.data;
//            console.debug("%@ alert\n%@".fmt(APP_EVENT_SOURCE_URL, message));
            alert(message);
        }, false);

        source.addEventListener('refresh', function(e) {
//            console.debug("%@ refresh".fmt(APP_EVENT_SOURCE_URL));
            location.reload();
        }, false);

        set(this, '_source', source);
    }
});


App.IndexRoute = Ember.Route.extend({
    model: function() {
        return get(this, 'state');
    },

    setupController: function(controller) {
        this._super.apply(this, arguments);
        var events = get(controller, 'events');
        events.connect();
    }
});


App.IndexController = Ember.ObjectController.extend({
    style: function() {
        var background = get(this, 'background'),
            color = get(this, 'color'),
            style = "";
        if (!isNone(background)) {
            style += "background-color: %@;".fmt(background);
        }
        if (!isNone(color)) {
            style += "color: %@;".fmt(color);
        }
        return style;
    }.property('background', 'color')
});


Ember.Handlebars.registerBoundHelper('formatDate', function(date) {
    if (isNone(date)) {
        return null;
    }

    function zeroPad(n, digits) {
        var s = n.toString(),
            zeroString = "0";
        while (s.length < digits) {
            s = zeroString + s;
        }
        return s;
    }

    return "%@-%@-%@ %@:%@:%@".fmt(
        zeroPad(date.getFullYear(), 4),
        zeroPad(date.getMonth() + 1, 2),
        zeroPad(date.getDate(), 2),
        zeroPad(date.getHours(), 2),
        zeroPad(date.getMinutes(), 2),
        zeroPad(date.getSeconds(), 2)
    );
});

App.ready = function() {
    App.register("events:default", App.Events);
    App.register("state:default", App.State);
    App.inject("controller", 'events', "events:default");
    App.inject("events:default", 'state', "state:default");
    App.inject("route:index", 'state', "state:default");

    applicationCache.addEventListener('updateready', function() {
        location.reload();
    }, false);
};

})(this);
