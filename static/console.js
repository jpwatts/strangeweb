(function(global, undefined) {

var get = Ember.get,
    set = Ember.set,
    isEmpty = Ember.isEmpty,
    isNone = Ember.isNone,
    typeOf = Ember.typeOf;


var Console = global.Console = Ember.Application.create();


Console.Socket = Ember.Object.extend(Ember.Evented, {
    _socket: null,

    reconnectWait: 1000 * 5,  // five seconds

    connect: function() {
        var socket = get(this, '_socket');

        if (! isNone(socket)) {
            return;
        }

        var url = "%@://%@%@".fmt(
            location.protocol === "https:" ? "wss" : "ws",
            location.host,
            Console.socketUrl
        );

        var _this = this;

        socket = new WebSocket(url);

        socket.addEventListener('open', function() {
//            console.debug("OPEN %@".fmt(url));
        }, false);

        socket.addEventListener('message', function(e) {
//            console.debug("MESSAGE %@\n%@".fmt(url, e.data));
            var data = JSON.parse(e.data);
            _this.trigger('data', data);
        }, false);

        socket.addEventListener('error', function() {
//            console.debug("ERROR %s".fmt(url));
            _this.reconnect();
        }, false);

        socket.addEventListener('close', function() {
//            console.debug("CLOSE %@".fmt(url));
            _this.reconnect();
        }, false);

        set(this, '_socket', socket);
    },

    reconnect: function() {
        var wait = get(this, 'reconnectWait');
        set(this, '_socket', null);
        Ember.run.later(this, this.connect, wait);
    },

    send: function(command) {
        var socket = get(this, '_socket'),
            text = get(command, 'text');
        socket.send(text);
    },

    isConnected: function() {
        var socket = get(this, '_socket');
        return ! isNone(socket);
    }.property('_socket')
});


Console.Command = Ember.Object.extend({
    text: null,
    data: null,

    isError: function() {
        return "error" === get(this, 'data.stream');
    }.property('data.stream'),

    result: function() {
        var data = get(this, 'data');
        return JSON.stringify(data, undefined, 4);
    }.property('data')
});


Console.IndexRoute = Ember.Route.extend({
    model: function() {
        return [];
    },

    setupController: function(controller) {
        this._super.apply(this, arguments);
        var socket = get(controller, 'socket');
        socket.connect();
        socket.on('data', this, function(data) {
            var command = get(controller, 'firstObject');
            set(command, 'data', data);
            set(controller, 'isBusy', false);
        });
    }
});


Console.IndexController = Ember.ArrayController.extend({
    commandText: null,
    isBusy: false,

    actions: {
        runCommand: function() {
            set(this, 'isBusy', true);
            var text = get(this, 'commandText');
            if (isEmpty(text)) {
                return;
            }
            var command = Console.Command.create({text: text}),
                socket = get(this, 'socket');
            socket.send(command);
            this.unshiftObject(command);
            set(this, 'commandText', null);
        }
    },

    inputIsDisabled: function() {
        var isBusy = get(this, 'isBusy'),
            isConnected = get(this, 'socket.isConnected');
        return isBusy || !isConnected;
    }.property('isBusy', 'socket.isConnected')
});


Console.ready = function() {
    Console.register("socket:default", Console.Socket);
    Console.inject("controller", 'socket', "socket:default");
};

})(this);
