(function(global, undefined) {

var get = Ember.get,
    set = Ember.set,
    isEmpty = Ember.isEmpty;


var Admin = global.Admin = Ember.Application.create();


Admin.IndexController = Ember.Controller.extend({
    data: null,
    isBusy: false,

    actions: {
        clearMessage: function() {
            set(this, 'isBusy', true);
            var _this = this;
            $.ajax({
                url: Admin.restUrl,
                type: "DELETE"
            }).then(function() {
                set(_this, 'isBusy', false);
            });
            set(this, 'data', null);
        },

        showMessage: function() {
            var data = get(this, 'data');
            if (isEmpty(data)) {
                return;
            }
            set(this, 'isBusy', true);
            var _this = this;
            $.ajax({
                url: Admin.restUrl,
                data: JSON.stringify(data),
                contentType: "application/json; charset=UTF-8",
                type: "PUT"
            }).then(function() {
                set(_this, 'isBusy', false);
            });
            set(this, 'data', null);
        }
    },

    showMessageIsDisabled: function() {
        var data = get(this, 'data');
        return get(this, 'isBusy') || isEmpty(data);
    }.property('data', 'isBusy')
});

})(this);
