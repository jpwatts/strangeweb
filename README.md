# Strangeweb

This project goes along with a talk I'm giving about using websockets and server-sent events to push data to client apps running in the browser. It will be available during the talk at [strangeweb.joelwatts.com](http://strangeweb.joelwatts.com/).


## Interfaces

### [/](http://strangeweb.joelwatts.com/)

The dashboard shows a message from a user of the site. It also shows a count of active connections and a time stamp indicating when the last change to the site was made.

Under the hood it's an [Ember](http://emberjs.com/) app subscribed to a [server-sent events](https://developer.mozilla.org/en-US/docs/Server-sent_events/Using_server-sent_events) source. The dashboard works offline using the [HTML5 application cache](https://developer.mozilla.org/en-US/docs/HTML/Using_the_application_cache).

### [/admin](http://strangeweb.joelwatts.com/admin)

The admin provides an interface to update the message on the dashboard. It's also an Ember app and talks to the server using a simple REST endpoint.

### [/console](http://strangeweb.joelwatts.com/console)

The console is another Ember app, connected to a [websocket](https://developer.mozilla.org/en-US/docs/WebSockets), that provides a command-line interface for site administrators.


## Data

### [/events](http://strangeweb.joelwatts.com/events)

The events endpoint streams server-sent events when application state changes.

    $ curl --no-buffer "http://strangeweb.joelwatts.com/events"

### [/data](http://strangeweb.joelwatts.com/data)

The data endpoint returns the current message.

    $ curl "http://strangeweb.joelwatts.com/data"

You can change it by sending a `PUT` with a JSON-encoded message.

    $ curl -X PUT -d '"Dr. Strangeweb"' "http://strangeweb.joelwatts.com/data"

Or remove the message with `DELETE`.

    $ curl -X DELETE "http://strangeweb.joelwatts.com/data"

### [/data/stream](http://strangeweb.joelwatts.com/data/stream)

The data stream endpoint streams messages as they change.

    $ curl --no-buffer "http://strangeweb.joelwatts.com/data/stream"

### [/console/socket](http://strangeweb.joelwatts.com/console/socket)

The console socket provides the websocket used by the console.

    $ wscat --connect "ws://strangeweb.joelwatts.com/console/socket"
