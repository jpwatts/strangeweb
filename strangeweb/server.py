import datetime
import os
import shlex
import traceback

from tornado import ioloop, web, websocket
from tornado.options import options

from .state import decode, encode, manager


ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))

CACHE_MANIFEST = "text/cache-manifest; charset=UTF-8"
EVENT_STREAM = "text/event-stream; charset=UTF-8"
JSON = "application/json; charset=UTF-8"

KEEP_ALIVE = 1000 * 30  # 30 seconds


class AppHandler(web.RequestHandler):
    def get(self):
        return self.render("app.html", state=manager.state)

    def head(self):
        pass


class ManifestHandler(web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Content-Type', CACHE_MANIFEST)

    def get(self):
        self.render("cache.manifest", state=manager.state)

    def head(self):
        pass


class AdminHandler(web.RequestHandler):
    def get(self):
        return self.render("admin.html", state=manager.state)

    def head(self):
        pass


class DataHandler(web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Content-Type', JSON)

    def get(self):
        data = manager.state.get('data')
        self.write("{}\n".format(encode(data)))

    def head(self):
        pass

    def delete(self):
        data = None
        manager.state['data'] = data
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()
        self.write("{}\n".format(encode(data)))

    def put(self):
        body = self.request.body.decode('utf-8').strip()
        try:
            data = decode(body)
        except ValueError:
            raise web.HTTPError(400)
        manager.state['data'] = data
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()
        self.write("{}\n".format(encode(data)))


class StreamingDataHandler(web.RequestHandler):
    callback = None
    handlers = set()

    def set_default_headers(self):
        self.set_header('Content-Type', JSON)

    @web.asynchronous
    def get(self):
        self.handlers.add(self)
        manager.state['connections'] += 1
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def head(self):
        pass

    def on_connection_close(self):
        self.handlers.remove(self)
        manager.state['connections'] -= 1
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()

    @classmethod
    def on_state(cls, state, force=False):
        data = state.get('data')
        message = "{}\r\n".format(encode(data))
        for handler in cls.handlers:
            handler.write(message)
            handler.flush()

    @classmethod
    def keep_alive(cls):
        cls.on_state(manager.state)

    @classmethod
    def start(cls):
        callback = ioloop.PeriodicCallback(cls.keep_alive, KEEP_ALIVE)
        callback.start()
        cls.callback = callback

    @classmethod
    def stop(cls):
        cls.callback.stop()
        cls.callback = None


class EventsHandler(web.RequestHandler):
    callback = None
    handlers = set()

    def set_default_headers(self):
        self.set_header('Content-Type', EVENT_STREAM)

    @web.asynchronous
    def get(self):
        self.handlers.add(self)
        manager.state['connections'] += 1
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def head(self):
        pass

    def on_connection_close(self):
        self.handlers.remove(self)
        manager.state['connections'] -= 1
        manager.state['time'] = datetime.datetime.utcnow()
        manager.publish()

    @classmethod
    def on_state(cls, state):
        cls.event("state", encode(state))

    @classmethod
    def event(cls, name, message=None):
        encoded_message = ""
        if name:
            encoded_message += "event: {}\n".format(name)

        if message:
            for line in message.splitlines():
                encoded_message += "data: {}\n".format(line)
        else:
            encoded_message += "data:\n"
        encoded_message += "\n"

        for handler in cls.handlers:
            handler.write(encoded_message)
            handler.flush()

    @classmethod
    def keep_alive(cls):
        cls.on_state(manager.state)

    @classmethod
    def start(cls):
        callback = ioloop.PeriodicCallback(cls.keep_alive, KEEP_ALIVE)
        callback.start()
        cls.callback = callback

    @classmethod
    def stop(cls):
        cls.callback.stop()
        cls.callback = None


class ConsoleHandler(web.RequestHandler):
    def get(self):
        return self.render(
            "console.html",
            help="Commands: {}".format(", ".join(ConsoleSocketHandler.get_commands())),
            state=manager.state
        )

    def head(self):
        pass


class ConsoleSocketHandler(websocket.WebSocketHandler):
    class CommandError(Exception):
        pass

    @classmethod
    def get_commands(cls):
        commands = []
        for name in dir(cls):
            if not name.startswith('command_'):
                continue
            commands.append(name[8:])
        return sorted(commands)

    def command_alert(self, command, message):
        self.command_event(command, 'alert', message)

    def command_event(self, command, name, message):
        EventsHandler.event(name, message)

    def command_refresh(self, command):
        self.command_event(command, 'refresh', None)

    def command_state(self, command, *args):
        usage = "Usage: state get|set|delete <key> [<value>]"
        state = manager.state
        args = list(args)
        arg_count = len(args)

        if not arg_count:
            return state

        if arg_count > 3:
            raise self.CommandError(usage)

        sub_command = args.pop(0).lower()

        if sub_command not in ['get', 'set', 'delete']:
            raise self.CommandError(usage)

        key = args.pop(0)
        arg_count = len(args)

        if "set" == sub_command:
            if 1 != arg_count:
                raise self.CommandError(usage)
            state[key] = decode(args[0])
            if 'time' != key:
                state['time'] = datetime.datetime.utcnow()
            manager.publish()
            return

        if arg_count:
            raise self.CommandError(usage)

        if key not in state:
            raise self.CommandError("That key doesn't exist.")

        if "delete" == sub_command:
            del state[key]
            if 'time' != key:
                state['time'] = datetime.datetime.utcnow()
            manager.publish()
            return

        return state[key]

    def command_background(self, command, color=None):
        state = manager.state
        if color is None:
            return state.get('background')
        color = color.lower().split(';')[0].strip()
        if "clear" == color:
            try:
                del state['background']
            except KeyError:
                pass
        else:
            state['background'] = color
        state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def command_color(self, command, color=None):
        state = manager.state
        if color is None:
            return state.get('color')
        color = color.lower().split(';')[0].strip()
        if "clear" == color:
            try:
                del state['color']
            except KeyError:
                pass
        else:
            state['color'] = color
        state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def command_connections(self, command):
        return manager.state.get('connections')

    def command_data(self, command, data=None):
        state = manager.state
        if data is None:
            return state.get('data')
        state['data'] = data
        state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def command_time(self, command):
        return manager.state.get('time')

    def command_help(self, command):
        return self.get_commands()

    def command_version(self, command, version=None):
        state = manager.state
        if version is None:
            return state.get('version')
        state['version'] = version
        state['time'] = datetime.datetime.utcnow()
        manager.publish()

    def error(self, message=None, exception=None):
        data = dict(stream="error")
        if exception is None:
            data['traceback'] = None
        else:
            data['traceback'] = traceback.format_exc()
            if message is None:
                message = exception
        data['message'] = str(message)
        self.write_message(encode(data))

    def out(self, result):
        data = dict(stream="out", result=result)
        self.write_message(encode(data))

    def on_message(self, message):
        message = message.strip()

        if not message:
            return

        argv = shlex.split(message)

        try:
            command = getattr(self, 'command_{}'.format(argv[0]))
        except AttributeError:
            self.error("That isn't a command.")
            return

        try:
            result = command(*argv)
        except self.CommandError as e:
            self.error(e)
            return
        except Exception as e:
            self.error(exception=e)
            return

        self.out(result)


def main():
    options.define('address', default="127.0.0.1", help="Set the IP address")
    options.define('debug', default=False, type=bool, help="Run in debug mode")
    options.define('port', default=8000, type=int, help="Set the port")
    options.parse_command_line()

    application = web.Application(
        [
            (r'^/$', AppHandler, None, 'app'),
            (r'^/manifest$', ManifestHandler, None, 'manifest'),
            (r'^/events$', EventsHandler, None, 'events'),
            (r'^/data', DataHandler, None, 'data'),
            (r'^/data/stream$', StreamingDataHandler, None, 'data.stream'),
            (r'^/admin$', AdminHandler, None, 'admin'),
            (r'^/console', ConsoleHandler, None, 'console'),
            (r'^/console/socket$', ConsoleSocketHandler, None, 'console.socket'),
        ],
        debug=options.debug,
        static_path=os.path.join(ROOT, 'static'),
        template_path=os.path.join(ROOT, 'templates')
    )
    application.listen(options.port, options.address)

    manager.register(StreamingDataHandler.on_state)
    StreamingDataHandler.start()

    manager.register(EventsHandler.on_state)
    EventsHandler.start()

    io_loop = ioloop.IOLoop.current()
    try:
        io_loop.start()
    except KeyboardInterrupt:
        io_loop.stop()
