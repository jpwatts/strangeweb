import datetime
import json
import uuid


__all__ = [
    'decode',
    'encode',
    'manager',
]


def decode(s):
    return json.loads(s)


def encode(d):

    class DateTimeAwareJSONNEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, datetime.datetime):
                return u"{}Z".format(obj.isoformat())
            if isinstance(obj, datetime.date):
                return obj.isoformat()
            return super(DateTimeAwareJSONNEncoder, self).default(obj)

    return json.dumps(d, cls=DateTimeAwareJSONNEncoder, separators=(',', ':'), sort_keys=True)


class StateManager(object):
    def __init__(self):
        self.callback = None
        self.registry = set()
        self.state = dict(
            connections=0,
            data=None,
            time=datetime.datetime.utcnow(),
            version=uuid.uuid4().hex
        )

    def publish(self):
        state = self.state
        for func in self.registry:
            func(state)

    def register(self, func):
        self.registry.add(func)


manager = StateManager()
